# API Documentation
This is the upstream repository for the api documentations application. Add your api documentation specs here to make it available in web.

## Guidelines
- Documentation needs to be written in [OpenAPI Specification 3.x.x (OAS 3)](https://spec.openapis.org/oas/v3.1.0).
- You can make use of [Redoc specific parameters](https://github.com/Redocly/redoc#swagger-vendor-extensions) to make documentation more inviting.
- Accepted format - YAML or JSON
- Update the `__toc__.yml` file to make the specification available to world.
- `__config__.yml` file defines the configuration.

```yml
# Sample __toc__.yml

- name: First Title                       # need not to be unique
  version: version_1.0.1                  # combination of name & version must be unique
  description: This is small description  # description of the application
  image: /images/first-image.png          # hero image of the application
  href: first-title.yml                   # file name with extension for openapi spec | unique
  alert:                                  # optional
    type: warning                         # accepted value - warning, info
    message: This version is deprecated. Please migrate to version_2.0.1
```

## Issues
In case of any problem in the application open an issue here at https://gitlab.com/intuginetech/common/api-documentation/issues

## License

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a>

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.
